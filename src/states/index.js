import { combineReducers } from "redux";

// Register your redux store under a unique namespace
export const namespace = "autoanswer";

// Combine the reducers
export default combineReducers({});
